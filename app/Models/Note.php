<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    //para que laravel no muestre un problema por una asignación masiva
    //se agrega este campo protegido que le indique que datos entran
    protected $fillable = [
        'excerpt',
        'content'
    ];
}

//comentario prueba
