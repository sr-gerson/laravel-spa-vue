<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Note;
use Illuminate\Http\Request;
//controladores
class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //listar
    public function index(Request $request)
    {
        //Inertia la que permite conectar a laravel con vue
        //con eloquent paso a la vista un array de notes que me traiga los ultimas notas.
        //y hace una busqueda en el  modelo con los extractos que ingrese el usuario
        return Inertia::render('Notes/Index', [
            'notes' => Note::latest()
                ->where('excerpt', 'LIKE', "%$request->q%")
                ->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //crear
    public function create()
    {
        return Inertia::render('Notes/Create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //salvar
    public function store(Request $request)
    {
        $request->validate([
            'excerpt' => 'required',
            'content' => 'required',
        ]);

        $note = Note::create($request->all());

        return redirect()->route('notes.edit', $note->id)->with('status', 'Nota creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    //mostrar
    public function show(Note $note)
    {
        //muestra una nota en especifico. con compact mandmos un objeto al componente
        return Inertia::render('Notes/Show', compact('note'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    //editar
    public function edit(Note $note)
    {
        return Inertia::render('Notes/Edit', compact('note'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    //actualizar
    public function update(Request $request, Note $note)
    {
        //validar campos que llegan desde el formulario
        $request->validate([
            'excerpt' => 'required',
            'content' => 'required',
        ]);

        //despues de ser validada. actualize con todos los campos que manda el usuario
        $note->update($request->all());

        return redirect()->route('notes.index')->with('status', 'Nota actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Note  $note
     * @return \Illuminate\Http\Response
     */
    //eliminar
    public function destroy(Note $note)
    {
        $note->delete();

        return redirect()->route('notes.index')->with('status', 'Nota eliminada');
    }
}
